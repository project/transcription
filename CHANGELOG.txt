
2008-08
-------
  * Placeholders in t() for form descriptions (aaron).
  * Change various t()'s to check_plain() (aaron).
  * Change Widget label to Media Transcription (aaron).
  * Change Field Info label to Media Transcription (aaron).
  * Change module name to Media Transcriptions (aaron).
  * Change package to CCK (aaron).
  * Remove filter_xss wrappers around t() (aaron).
  * Create CCK field for d5 version (grndlvl/aaron).
  * Initial creation (aaron).
