<?php
drupal_set_header('Content-Type: text/xml; charset=utf-8');
$output = '';
$output .= '<tt xml:lang="en" xmlns="http://www.w3.org/2006/10/ttaf1" xmlns:tts="http://www.w3.org/2006/10/ttaf1#style">'. "\n";
$output .= '<head>'. "\n";
$output .= '<layout />'. "\n";
$output .= '</head>'. "\n";
$output .= '<body>'. "\n";
$output .= '<div xml:id="captions">'. "\n";
foreach ($transcriptions as $transcription) {
  $output .= '<p begin="'. theme('transcription_seconds_to_time', $transcription['begin_timemarker']) 
    .'" end="'. theme('transcription_seconds_to_time', $transcription['end_timemarker']) .'">'. check_plain($transcription['transcription']) .'</p>'. "\n";
}
$output .= '</div>'. "\n";
$output .= '</body>'. "\n";
$output .= '</tt>';